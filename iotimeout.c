#include <stdio.h>
#include <stdlib.h>

#include <libyottadb.h>

#define BUFFER_SIZE 2048
#define INIT_YDB_BUFFER(buffer, len) (buffer)->buf_addr = malloc(len); (buffer)->len_used = 0; (buffer)->len_alloc = len;

int main(int argc, char **argv)
{
	ydb_buffer_t global_name, latest_session_id, session_id, z_status, z_status_value;
	char buff[BUFFER_SIZE];
	int status;

	YDB_LITERAL_TO_BUFFER("$ZSTATUS", &z_status);
	YDB_LITERAL_TO_BUFFER("^sessions", &global_name);
	YDB_LITERAL_TO_BUFFER("id", &latest_session_id);
	INIT_YDB_BUFFER(&z_status_value, BUFFER_SIZE);
	INIT_YDB_BUFFER(&session_id, BUFFER_SIZE);

	status = ydb_incr_s(&global_name, 1, &latest_session_id, NULL, &session_id);
        if(status != YDB_OK)
        {
                status = ydb_get_s(&z_status, 0, NULL, &z_status_value);
                z_status_value.buf_addr[z_status_value.len_used] = '\0';
                fprintf(stderr, "Error with YDB: %s\n", z_status_value.buf_addr);
        }

	fgets(buff, BUFFER_SIZE, stdin);
	printf("Got string %s\n", buff);

	return 0;
}
